# README #

Corewar a 42 school project.

### What is this repository for? ###

The objective of this project is to develope an arena "virtual machine" where programs "Champions" will confront each other to death
and eventually develop an assemler to compile the champions, and make our own champion as well.

### How do I get set up? ###

1. Compile the files to create "./corewar" and "./asm" :
	make

2. Within the directory Champs you can find the players

usage: ./corewar [-dump N -s N -v N] [-a] [[-n N] <champion1.cor>] <...>
 -a           : Print output from "aff"(Default is to hide it)
#### TEXT OUTPUT MODE ##########################################################
 -d (-dump) N : Dumps memory after N cycles then exits
 -s N         : Runs N cycles, dumps memory, pauses, then repeats
 -v N         : Verbosity levels, can be added together to enable several
                - 0  : Show only essentials
                - 1  : Show lives
                - 2  : Show cycles
                - 4  : Show operations (Params are NOT litteral ...)
                - 8  : Show deaths
                - 16 : Show PC movements (Except for jumps)

### Who do I talk to? ###

	If you have any quetions: abnaceur@student.42.fr
		or contact@naceur-abdeljalil.com